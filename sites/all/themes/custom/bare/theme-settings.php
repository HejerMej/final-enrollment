<?php 
include_once 'template.php';

/**
 * Advanced theme settings.
 */
function bare_form_system_theme_settings_alter(&$form, $form_state) {
  
  $form['menu'] = array(
    '#type'        => 'fieldset', 
    '#title'       => t('Primary menu settings'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  
  // Menu elements
  $form['menu']['menu_type'] = array(
    '#type'        => 'select',
    '#title'       => t('Which kind of primary links do you want to use?'),
    '#description' => t('Classic one-level primary links, or mega drop-down menu'),
    '#options'     => array(
      1 => t('Classic Primary Links'),
      2 => t('Mega Drop Down'),
    ),
    '#default_value' => theme_get_setting('menu_type'),
  );
  
  $form['menu']['menu_element'] = array(
    '#type'          => 'select',
    '#title'         => t('Megamenu Source'),
    '#description'   => t('Choose a menu to render as megamenu'),
    '#options'       => menu_get_menus(),
    '#default_value' => theme_get_setting('menu_element'),
  );

  $form['#submit'][] = 'bare_settings_submit';
  return $form;
}

/**
 * Save settings data.
 */
function bare_settings_submit($form, &$form_state) {
  $settings = array();

}

/**
 * Provvide default installation settings for bare.
 */
function _bare_install() {
  // Flag theme is installed
  variable_set('theme_bare_first_install', FALSE);
}