  <?php

  /**
   * Use of block off based markup should use BEM Syntax, be sure to style accordingly
   *   http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/
   *  http://coding.smashingmagazine.com/2012/04/16/a-new-front-end-methodology-bem/
   *
   * Use Gumby Framework for all css and html markup
   *  http://gumbyframework.com
   *
   * -- Jamie smith
   *  
   */

  ?>
<!--Head of page-->
<div class="head">
  <div class="row">
    <header class="twelve coloumns">
    <h1>USAP HEALTH CARD</h1>
    <h2>Insurance is provided on a guaranteed issue basis. No one can be denied coverage.</h2>
    </header>
  </div>
</div>
<!--end head-->
  
<div class="content">
    <?php print render($page['content']); ?>
  </div>

<div class="wrapper">

<!--upgrade-general-info-payment, left column-->


  <div class="row top">
        

      <ul class="seven columns form">
        <li class="clearfix">
          <section class="info-box">
            <h3 class="active-info-title">1. Upgrade Your Coverage</h3>

            <div class="info-box__content upgrades">
              <p class="sub-title">Additional insurance coverage is provided at a monthly rate of 67 ¢ per $10,000 of coverage. Summary of Voluntary Coverage</p>
              <ul class="four_up coverage-option">
                <li class="coverage-option"><span class="price-option">$50,000</span><input class="add-btn" type="submit" value="Add" /></li>
                <li class="coverage-option"><span class="price-option">$100,000</span><input class="add-btn" type="submit" value="Add" /></li>
                <li class="coverage-option"><span class="price-option">$150,000</span><input class="add-btn" type="submit" value="Add" /</li>
                <li class="coverage-option"><span class="price-option">$200,000</span><input class="add-btn" type="submit" value="Add" /></li>
              </ul>
              <p style="text-align:center; margin:15px 0 15px 0;"><a class="sub-link" href="#">No Thanks, I'll take the free 3,000 in coverage</a></p>

            </div>
          </section>
        </li>
        <li class="clearfix">
          <section class="info-box">
            <h3 class="inactive-info-title">2. Create Your Account</h3>




          <div class="info-box__content create-account">
                <div class="six columns left">
                  <ul>
                        <li class="field">
                        <p class="input-title">First Name</p>
                         <input class="input" type="text" placeholder="" />
                        </li>
                        <li class="field">
                        <p class="input-title">Last Name</p>
                         <input class="input" type="text" placeholder="" />
                        </li>
                        <li class="field">
                        <p class="input-title">Street</p>
                         <input class="input" type="text" placeholder="" />
                        </li>
                        <li class="field">
                        <p class="input-title">City</p>
                         <input class="input" type="text" placeholder="" />
                        </li>
                      <li class="field">
                          <p class="input-title">State</p>
                        <div class="picker">
                       
                          <select>
                            <option value="#" disabled>-</option>
                            <option>NC</option>
                            <option>GA</option>
                            <option>SC</option>
                            <option>AL</option>
                            <option>FL</option>
                            <option>CA</option>
                          </select>
                        </div>
                      </li>
                        <li class="field">
                        <p class="input-title">Zip</p>
                         <input class="input" type="text" placeholder="" />
                        </li>
                  </ul>
                </div>


                        <div class="five columns right">
                          <ul>
                            <li class="field">
                            <p class="input-title">Email Address</p>
                            <input class="input" type="email" placeholder="" />
                            </li>
                            
                            <li class="field">
                            <p class="input-title">Phone Number</p>
                            <input class="input" type="text" placeholder="" />
                            </li>
                            
                            <li class="field">
                            <p class="input-title">Birthday</p>
                            <input class="input" type="text" placeholder="" />
                            </li>
                            

                            <li class="field">
                            <label class="radio checked gender" for="radio1">
                            <input name="radio" id="radio1" value="1" type="radio" checked="checked">
                            <span class="gender-select"></span> Male
                            </label>
                            <label class="radio gender" for="radio2">
                            <input name="radio" id="radio2" value="2" type="radio">
                            <span class="gender-select"></span> Female
                            </label>
                            </li>
                          </ul>
                          
                          
                        </div>
                
                                  

                    <div class="clearfix"></div>  


                          <div class="row poop-wrapper">
                            <div class="one columns">
                              <ul>
                                <li class="field">
                                <label class="checkbox checked" for="check1">
                                <input name="checkbox[]" id="check1" value="1" type="checkbox" checked="checked">
                                <span></span>
                                </label>
                                </li>
                              </ul>
                            </div>
                          
                            <div class="ten columns">
                              <p class="disclaimer">By Providing This Information I am Agreeing to Enroll in the American Advantage Association’s USAP Health Card. To receive the benefits of the American Advantage Association's USAP Health Card, I acknowledge that I am 18 years of age (or older) and that I have read, understood and agree to the membership terms and conditions.</p>
                              <input class="add-btn-next" type="submit" value="Next" />
                            </div>
                          </div>

                  </div>
          </section>
        </li>
          <li class="clearfix">
              <section class="info-box">
                <h3 class="inactive-info-title">3. Payment</h3>
                
                  <div class="info-box__content payment">
                    
                      <div class="eleven columns">
                        <ul>
                          <li class="field">
                          <label class="radio checked gender" for="radio1">
                          <input name="radio" id="radio1" value="1" type="radio" checked="checked">
                          <span class="gender-select"></span> Credit Card
                          </label>
                          
                          <label class="radio gender" for="radio2">
                          <input name="radio" id="radio2" value="2" type="radio">
                          <span class="gender-select"></span> Check
                          </label>
                          </li>
                          
                          <li class="field">
                          <p class="input-title">Credit Card</p>
                          <input class="input" type="text" placeholder="" />
                          </li>
                          
                          <li class="field">
                          <p class="input-title">Card Number</p>
                          <input class="input" type="text" placeholder="" />
                          </li>
                          
                          <li class="field">
                          <p class="input-title">Experation Month</p>
                          <input class="input" type="number" placeholder="" />
                          </li>
                          
                          <li class="field">
                          <p class="input-title">Experation Day</p>
                          <input class="input" type="number" placeholder="" />
                          </li>
                          
                          
                          <li class="field">
                          <p class="input-title">City</p>
                          <input class="input" type="text" placeholder="" />
                          </li>
                        </ul>
                        <p class="signature-line">Please read and type name in E Signature Box</p>
                        <h5 class="esig">E Signature</h5>
                        <p class="signature-agree">I agree to enroll in the Federal Insurance Company Accident Insurance Plan, pay the insurance premium as part of the total monthly fees and I acknowledge that I have read, understood and agree to the Federal Insurance Company Limitations, Exclusions and Benefits Summary.      
                        I agree to enroll in the Federal Insurance Company Accident Insurance Plan, pay the insurance premium as part of the total monthly fees and I acknowledge that I have read, understood and agree to the insurance disclosures and authorizations .
                        All customers who complete and return the Enrollment Form will be subject to underwriting verification by Federal Insurance Company. This offer is not binding to the extent that United States trade or economic sanctions or other laws or regulations prohibit us from offering or providing insurance. To the extent any such prohibitions apply, this offer is void ab initio.
                        I hereby authorize the Insurance Company’s administrator NBFSA, LLC to deduct $3.33 for the first month of membership and insurance from my account and $3.33 on a monthly basis thereafter. I understand that this authority shall remain in force until I notify my bank or NBFSA, LLC in writing of its cancellation. I agree that this electronic signature has the same full legal force and effect as a handwritten signature or mark.
                        To electronically sign this form and submit, type your name in the signature box and click the "Enroll Now" button.
                        </p>
                          <ul>
                            <li class="field">
                            <input class="input" type="text" placeholder="" />
                            </li>
                          <input class="add-btn-next" type="submit" value="Enroll" style="margin-bottom:20px;" />
                          </ul>
                      </div>
                </div>
                
              </section>
          </li>
      </ul>

      

  <!--Summary Cart - Right Col-->
          <section class="five columns summary-box">
              <h3 class="summary-title">Summary</h3>
              <ul class="summary-box--container">
                <li class="summary-box--heading">Membership Program<p class="summary-box--result">Usap Health Card</p></li>
                <li class="summary-box--heading">Registration Fees<p class="summary-box--result">$0.00</p></li>
                <li class="summary-box--heading">Membership Fees<p class="summary-box--result">$0.00</p></li>
                <li class="summary-box--heading">Billing Cycle<p class="summary-box--result">monthly</p></li>
              </ul>
              
              <p class="price-total">Total Fees<span class="total-price"> $0.00</span></p>
              <p class="summary-disclaimer">Read the formal descriptions of the <a href="#">discount medical benefits.</a></p>
              <p class="summary-disclaimer">USAdvantagePlans are defined memberships in the <a href="#">American Advantage Association.</a></p>
          </section>
  </div>
  <!---end right col-->

<!--start of disclamer info-->
  <div class="row lower">
    <div class="seven columns">
      <p>The following are additional disclosures related to the discount medical benefits within your membership.</p>
      <p><span style="FONT-SIZE: 18.66px; font-weight:bold;">This is not insurance nor is it intended to replace insurance.</span> <span style="FONT-SIZE: 16.66px; font-weight:bold;">This discount card program contains a 30 day cancellation period.</span> The plan is not insurance coverage and does not meet the minimum creditable coverage requirements under the Affordable Care Act or Massachusetts M.G.L. c. 111M and 956 CMR 5.00. This plan provides discounts at certain healthcare providers for medical services. This plan does not make payments directly to the providers of medical services. The plan member is obligated to pay for all healthcare services but will receive a discount from those healthcare providers who have contracted with the discount plan organization. For a full list of disclosures, please <a href="http://content.newbenefits.com/feed.aspx?hash=1nCjynVyHgD3qMTJC7SQg">click here</a>. | <a href="http://content.newbenefits.com/feed.aspx?hash=6519gRcOdLk4PKnqDA">Terms and Conditions</a> | Discount Medical Plan Organization: New Benefits, Ltd., Attn: Compliance Department, PO Box 671309, Dallas, TX 75367-1309.</p>
    </div>
    <div class="four columns">
      <h4>Chubb has provided insurance services to individuals and businesses for over 128 years</h4>
      <p>and receives consistently high ratings for financial strength from A.M. Best, Moody's and Standard & Poor's, the leading independent analysts of the insurance industry. Chubb, Box 1615, Warren, N.J. 07061-1615.</p>
      <h4>The American Advantage Association and USAdvantagePlans</h4>
      <p>incur costs in connection with this sponsored program. To provide and maintain this valuable membership benefit, it is reimbursed for these costs. The American Advantage Association also receives a fee for the license of its name and logo used in connection with the program.</p>
    </div>
  </div>


</div> <!-- End of wrapper -->

<!--end first white disclamer info-->

<!--start of grey disclamer info-->
<div class="bottom-row">
  <div class="row">
    <div class="twelve columns bottom">
      <p>Free USAP Health Card and Premium Plans are not available in VT. Dental, Vision, Dental Vision and Wellness Plans not available in FL, KS, UT, VT, WA.</p>
      <p>Insurance is underwritten by Federal Insurance Company, a member insurer of the Chubb Group of Insurance Companies, Box 1615, Warren, N.J. 07061-1615. The coverage described in this literature may not be available in all jurisdictions. This literature is descriptive only. Actual coverage is subject to the language of the policies as issued. Exclusions & Limitations apply. See: <a href='http://usadvantagesavings.com/misc/NoCostCoverageSummary.pdf'>Summary of No Cost Coverage for details on the automatic $3,000 AD&D coverage<a/> and <a href='http://usadvantagesavings.com/misc/VoluntaryInsuranceSummary.pdf'>Summary of Voluntary Coverage</a> for details. Administered by NBFSA, LLC, PO Box 24279, Winston Salem, NC 27114, 888-424-4186. You have a 30-day free look period: if you are not happy with the insurance for any reason, and you have not submitted a claim that has been paid, simply return the Description of Coverage within the first 30 days after enrollment for a complete refund of your premium and the policy will be cancelled back to its effective date. To speak with a licensed insurance Agent, please contact Trupoint Marketing (a licensed insurance agency) at 888-424-4186.</p>
    </div>
  </div>
</div>
<!--end of grey disclamer info-->

<!--footer-->
<div class="footer">
  <footer class="row">
     <p class="copyright">USAP © 2013</p>    
     <p class="terms"><a href="#"?>Terms of Service</a></p>
     <p class="privacy"><a href="#">Privacy Policy</a></p>
     <p class="summary-disclaimer">Read the formal descriptions of the <a href="#">discount medical benefits.</a></p>
     <p class="summary-disclaimer">USAdvantagePlans are defined memberships in the <a href="#">American Advantage Association.</a></p>
  </footer>
</div>
<!--end footer-->

</div>

