<?php
?>
<!--Head of page-->
<div class="head">
    <div class="row">
      <header class="twelve coloumns">
        <h1><?php print $node->field_display_title['und'][0]['value']; ?></h1>
        <?php print ($node->field_subhead['und'][0]['value']) ? "<h2>" . $node->field_subhead['und'][0]['value'] . "</h2>" : '' ?>
        </header>
    </div>
</div>
<!--end head-->

<div class="wrapper">
<!--upgrade-general-info-payment, left column-->
    <div class="row top">
            <section class="six columns form">
                <?php
                    $block = module_invoke('webform', 'block_view', 'client-block-' . $node->field_webform['und'][0]['nid']);
                    print render($block['content']);
                ?>
            </section>
            <div class="one columns">
            </div>
    <!--Summary Cart - Right Col-->
                <?php print $node->field_cart_info['und'][0]['value'];?>
    </div>
    <!---end right col-->

    <!--start of disclamer info-->
    <div class="row lower">
        <?php
            //pulling rss feeds for disclaimers on enrollment sites ***
            function do_post($url, $params){
                $response = "";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                $result = curl_exec($ch);
                curl_close($ch);
                return $response;
            }
             do_post("http://content.newbenefits.com/Feednocss.aspx", "hash=LpNdI9kj1DgKXDP85AJxNw&Section=summary");

        $disclaimer = node_load($node->field_disclaimer['und'][0]['nid']);
        print $disclaimer->body['und'][0]['value'];
        ?>
    </div>
</div> <!-- End of wrapper -->

<!--end first white disclamer info-->

<!--start of grey disclamer info-->
<div class="bottom-row">
    <div class="row">
        <?php
        //$disclaimer = node_load($node->field_disclaimer['und'][1]['nid']);
        //print $disclaimer->body['und'][0]['value'];
        ?>
    </div>
</div>
<!--end of grey disclamer info-->

<!--footer-->
<div class="footer">
    <footer class="row">
        <?php print ($node->field_footer['und'][0]['value']) ? $node->field_footer['und'][0]['value'] : '' ?>
    </footer>
</div>
<!--end footer-->

