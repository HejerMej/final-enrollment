<?php

?>


<!--Head of page-->
<div id="top-header">
		<div id="et-info">
			<span id="et-info-phone">888-424-4186</span>
			<a href="mailto:info@theautogroup.org">
				<span id="et-info-email">info@theautogroup.org</span>
			</a>
	</div>
</div>



<header>
<div class="row">
	<div class="logo-container three columns">
		<img src="http://theautogroup.org/wp-content/uploads/2017/01/Auto-Logo-02.png"/>
	</div>

<div id="et-top-navigation nine columns">
		<nav id="top-menu-nav">
			<ul id="top-menu" class="nav"><li id="menu-item-64" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-39 current_page_item menu-item-64"><a href="http://theautogroup.org/">Home</a></li>
			<li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63"><a href="http://theautogroup.org/about-us/">About Us</a></li>
			<li id="menu-item-62" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62"><a href="http://theautogroup.org/plans/">Plans</a></li>
			<li id="menu-item-60" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-60"><a href="http://theautogroup.org/member-benefits/">Member Benefits</a></li>
			<li id="menu-item-61" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61"><a href="http://theautogroup.org/tips/">Tips</a></li>
			<li id="menu-item-65" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-65"><a href="http://theautogroup.org/contact-us/">Contact Us</a></li>
			</ul>
		</nav>			
</div>


</div>
</header>

<!--end head-->

<div class="wrapper">
<!--upgrade-general-info-payment, left column-->
	<div class="row top">
			<section class="seven columns form">
				<?php
					$block = module_invoke('webform', 'block_view', 'client-block-' . $node->field_webform['und'][0]['nid']);
					print render($block['content']);
				?>
			</section>

	<!--Summary Cart - Right Col-->
				<?php print $node->field_cart_info['und'][0]['value'];?>
	</div>
	<!---end right col-->

	<!--start of disclamer info-->
	<div class="row lower">
		<?php
		$disclaimer = node_load($node->field_disclaimer['und'][0]['nid']);
		print $disclaimer->body['und'][0]['value'];
		?>
	</div>
</div> <!-- End of wrapper -->

<!--end first white disclamer info-->

<!--start of grey disclamer info-->
<div class="bottom-row">
	<div class="row">
		<?php
		//$disclaimer = node_load($node->field_disclaimer['und'][1]['nid']);
		//print $disclaimer->body['und'][0]['value'];
		?>
	</div>
</div>
<!--end of grey disclamer info-->

<!--footer-->
<div class="footer">
	<footer class="row">
		<?php print ($node->field_footer['und'][0]['value']) ? $node->field_footer['und'][0]['value'] : '' ?>
	</footer>
</div>
<!--end footer-->