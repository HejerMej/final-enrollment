<?php

?>

<!--Head of page-->

<header style="background-image:url('https://source.unsplash.com/category/buildings');">
<div class="tint">
		<div class="top-header row">
				<div class="logo-container centered eight columns">
					<img src="../sites/all/themes/custom/bare/img/truscript_logo.png"/>
					
				</div>
				<h4>The Next Innovation In Prescription Drug Shopping</h4>
		</div>

		<div class="row">
		
		</div>
		</div>
</header>



<!--end head-->

<div class="wrapper">

<!--upgrade-general-info-payment, left column-->
	<div class="row top">
			<section class="seven columns form">
				
			    <?php if ($_SESSION['return_error']){ ?>
						<div class="danger label" style="background-color:#ff0000; padding:1rem; color:#ffffff; text-align:center;">
						<?php print $_SESSION['return_error'];?>
						<?php unset($_SESSION['return_error']);?>
						</div>
			   <?php }?>
				
				
				<?php
					$block = module_invoke('webform', 'block_view', 'client-block-' . $node->field_webform['und'][0]['nid']);
					print render($block['content']);
				?>
			</section>

	<!--Summary Cart - Right Col-->
				<?php print $node->field_cart_info['und'][0]['value'];?>
	</div>
	<!---end right col-->

	<!--start of disclamer info-->
	<div class="row lower">
		<?php
		$disclaimer = node_load($node->field_disclaimer['und'][0]['nid']);
		print $disclaimer->body['und'][0]['value'];
		?>
	</div>
</div> <!-- End of wrapper -->

<!--end first white disclamer info-->

<!--start of grey disclamer info-->
<div class="bottom-row">
	<div class="row">
		<?php
		//$disclaimer = node_load($node->field_disclaimer['und'][1]['nid']);
		//print $disclaimer->body['und'][0]['value'];
		?>
	</div>
</div>
<!--end of grey disclamer info-->

<!--footer-->
<div class="footer">
	<footer class="row">
		<?php print ($node->field_footer['und'][0]['value']) ? $node->field_footer['und'][0]['value'] : '' ?>
	</footer>
</div>
<!--end footer-->