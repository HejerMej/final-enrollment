<?php if (!empty($pre_object)) print render($pre_object) ?>

<article class='<?php print $classes ?> row content__node' <?php print ($attributes) ?>>
  <?php if (!empty($title_prefix)) print render($title_prefix); ?>

  <?php if (!$page && !empty($title)): ?>
    <header>
      <h2 <?php if (!empty($title_attributes)) print $title_attributes ?>>
        <?php if (!empty($new)): ?><span class='new'><?php print $new ?></span><?php endif; ?>
        <a href="<?php print $node_url ?>"><?php print $title ?></a>
      </h2>
    </header>
  <?php endif; ?>

  <?php if (!empty($title_suffix)) print render($title_suffix); ?>

  <?php if (!empty($submitted)): ?>
    <div class='submitted clearfix'><?php print $submitted ?></div>
  <?php endif; ?>

  <?php if (!empty($content)): ?>
    <div class='content clearfix <?php if (!empty($is_prose)) print 'prose' ?>'>
      <?php print render($content) ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($links)): ?>
    <div class='links clearfix'>
      <?php print render($links) ?>
    </div>
  <?php endif; ?>
</article>

<?php if (!empty($post_object)) print render($post_object) ?>
